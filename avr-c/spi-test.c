// the sensor communicates using SPI, so include the library:
#include <SPI.h>

const int chipSelectPin = 10;

void setup() {
  Serial.begin(9600);

  // start the SPI library:
  SPI.begin();
  SPI.beginTransaction(SPISettings(2000000, MSBFIRST, SPI_MODE3));

  // initialize the  data ready and chip select pins:
  pinMode(chipSelectPin, OUTPUT);

  digitalWrite(chipSelectPin, LOW);
}

void write_pixel(int x, int y, int color) {
  SPI.transfer(0x01);
  unsigned int addr = y * 80 + x;
  SPI.transfer((addr & 0xff00) >> 8);
  SPI.transfer(addr & 0xff);
  SPI.transfer(color);
}

void write_many(int x, int y, int len, int colors[]) {
  SPI.transfer(0x02);
  unsigned int addr = y * 80 + x;
  SPI.transfer((addr & 0xff00) >> 8);
  SPI.transfer(addr & 0xff);
  SPI.transfer(len);
  for (int i = 0; i < len; i++) {
    SPI.transfer(colors[i]);
  }
}

void clear_screen() {
  for (int y = 0; y < 60; y++) {
    for (int x = 0; x < 80; x++) {
      write_pixel(x, y, 0);
    }
  }
}

void clear_screen_fast() {
  // TODO: weird behaviour. seems to skip every other line? we should do
  // this with an array that is 80 long
  int zeroes[160] = {0}; // 0
  for (int y = 0; y < 60; y++) {
    write_many(0, y, 160, zeroes);
  }
}

void fill_cols() {
  for (int y = 0; y < 60; y++) {
    for (int x = 0; x < 80; x++) {
      write_pixel(x, y, (x % 3) + 1);
    }
  }
}

void loop() {
  while (Serial.available() > 0) {
    int b = Serial.read();
    if (b == 'c') {
      clear_screen();
    } else if (b == 'a') {
      write_pixel(1, 0, 1);
      write_pixel(2, 2, 2);
    } else if (b == 's') {
      fill_cols();
    } else if (b == 'd') {
      clear_screen_fast();
    }
  }
}
