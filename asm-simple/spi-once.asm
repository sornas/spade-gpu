.include "m328Pdef.inc"

; SPI mode 3, i.e. CPOL=1, CPHA=1
; MSB first

.org 0x0000
  jmp setup

spi_wait_transmit:
  ; read SPI special register
  in r16, SPSR
  ; skip if bit is set: SPIF (SPI interrupt flag)
  sbrs r16, SPIF
  rjmp spi_wait_transmit
  ret

setup:
  ; SPI output pins (may be overriden by SPI setup)
  sbi DDRB, 5 ; sck
  sbi DDRB, 3 ; mosi
  sbi DDRB, 2 ; ss

  cbi PINB, 2 ; ss = 0

  ; SPCR (SPI control register):
  ; ---
  ; + SPIE = 0 (disable interrupts)
  ; + SPE  = 1 (enable)
  ; + DORD = 0 (MSB first)
  ; + MSTR = 1 (main)
  ; ---
  ; + CPOL = 1 (SCK high when idle)
  ; + CPHA = 1 (clock phase: sample on trailing / setup on leading)
  ;   (CPOL=1, CPHA=1 => SPI mode 3)
  ; + SPR1 = 0
  ; + SPR0 = 0
  ;   (SCK frequency = f_osc/4)
  ; ---
  ldi r16, 0b01011100
  out SPCR, r16

  ldi r16, 0b00000000
  out SPDR, r16
  call spi_wait_transmit

  ldi r16, 0b00010110
  out SPDR, r16
  call spi_wait_transmit

  ;ldi r16, 0x01
  ;out SPDR, r16
  ;call spi_wait_transmit
  ;ldi r16, 0x00
  ;out SPDR, r16
  ;call spi_wait_transmit
  ;ldi r16, 0x00
  ;out SPDR, r16
  ;call spi_wait_transmit
  ;ldi r16, 0x01
  ;out SPDR, r16
  ;call spi_wait_transmit

  ; fallthrough to main
main:
  rjmp main
