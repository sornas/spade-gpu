.include "m328Pdef.inc"

; d13 -- PB5

.org 0x0000
  jmp setup

setup:
  ; B5 as output
  sbi DDRB, 5

  ; B5 value
  sbi PORTB, 5

  ; fallthrough to main
main:
  rjmp main
