# top=main::spi_sub_recv
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import FallingEdge
from spade import SpadeExt

async def setup_spi(clk, s):
    s.i.rst = "true"
    s.i.sel = "false" # active low...
    s.i.sck = "false"
    s.i.sdi = "false"
    await cocotb.start(Clock(clk, 1, units="ns").start())
    await FallingEdge(clk) # propagate default values
    await FallingEdge(clk)
    await FallingEdge(clk)
    s.i.rst = "false"
    await FallingEdge(clk)
    s.o.assert_eq("None()")


async def send_bit(clk, s, b):
    s.i.sdi = b
    s.i.sck = "false"
    await FallingEdge(clk)
    await FallingEdge(clk)
    s.i.sck = "true"
    await FallingEdge(clk)
    await FallingEdge(clk)

async def send_word(clk, s, bb):
    for b in bb:
        await send_bit(clk, s, "true" if b == 1 else "false")

@cocotb.test()
async def one_word(dut):
    s = SpadeExt(dut)
    clk = dut.clk_i
    await setup_spi(clk, s)
    await send_word(clk, s, [0, 1, 0, 1, 1, 1, 0, 0])
    s.o.assert_eq("Some(92)") # 01011100

@cocotb.test()
async def many_words(dut):
    s = SpadeExt(dut)
    clk = dut.clk_i
    await setup_spi(clk, s)

    await send_word(clk, s, [0, 1, 0, 1, 1, 1, 0, 0])
    s.o.assert_eq("Some(92)") # 01011100
    await send_word(clk, s, [0, 1, 1, 0, 0, 1, 0, 0])
    s.o.assert_eq("Some(100)") # 01100100
    await send_word(clk, s, [0, 0, 0, 0, 0, 0, 0, 0])
    s.o.assert_eq("Some(0)") # 00000000
    await send_word(clk, s, [0, 0, 0, 0, 0, 0, 0, 1])
    s.o.assert_eq("Some(1)") # 00000001
    await send_word(clk, s, [1, 1, 1, 1, 1, 1, 1, 1])
    s.o.assert_eq("Some(255)") # 11111111


@cocotb.test()
async def all_words(dut):
    s = SpadeExt(dut)
    clk = dut.clk_i
    await setup_spi(clk, s)

    for n in range(256):
        bits = bin(n)[2:].zfill(8)
        await send_word(clk, s, map(int, bits))
        s.o.assert_eq("Some({})".format(n))
