# top=main::top
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import FallingEdge
from spade import SpadeExt

# s.i.switch: [rst, _, _, _]
# s.i.pmod_1: sel
# s.i.pmod_2: sdi
# s.i.pmod_3: sdo (not used yet)
# s.i.pmod_4: sck

async def setup(clk, s):
    s.i.switch = "[true, false, false, false]"
    s.i.pmod_1 = "false" # active low...
    s.i.pmod_4 = "false"
    s.i.pmod_2 = "false"
    await cocotb.start(Clock(clk, 1, units="ns").start())
    await FallingEdge(clk) # propagate default values
    await FallingEdge(clk)
    await FallingEdge(clk)
    s.i.switch = "[false, false, false, false]"
    await FallingEdge(clk)

async def send_bit(clk, s, b):
    s.i.pmod_2 = b
    s.i.pmod_4 = "false"
    await FallingEdge(clk)
    await FallingEdge(clk)
    s.i.pmod_4 = "true"
    await FallingEdge(clk)
    await FallingEdge(clk)

async def send_word(clk, s, bb):
    for b in bb:
        await send_bit(clk, s, "true" if b == 1 else "false")

@cocotb.test()
async def test_setup(dut):
    s = SpadeExt(dut)
    clk = dut.clk
    await setup(clk, s)
    await send_word(clk, s, [0, 1, 0, 1, 1, 1, 0, 0]) # 0101_1100: 92
    await send_word(clk, s, [0, 0, 1, 0, 1, 1, 0, 1])
    # to see links to the generated waveform:
    # assert False
