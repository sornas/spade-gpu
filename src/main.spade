use std::conv::concat;
use std::mem;
use std::ports::new_mut_wire;
use std::ports::read_mut_wire;

enum CommandProgress {
    // 0:
    Waiting,
    // 1:
    WritePixel {
        addr1: Option<uint<8>>,
        addr2: Option<uint<8>>,
    },
    WriteMany {
        addr1: Option<uint<8>>,
        addr:  Option<uint<16>>,
        len:   Option<uint<8>>,
    }
}

enum Command {
    WritePixel {
        addr: uint<16>,
        value: uint<4>,
    },
    Dummy,
}

entity command_fsm(
    clk: clock,
    rst: bool,
    data: Option<uint<8>>,
) -> Option<Command> {
    reg(clk) (progress, command) reset (rst: (CommandProgress::Waiting(), None())) = {
        match data {
            Some(data) => {
                match progress {
                    CommandProgress::Waiting => {
                        if data == 1 {
                            (CommandProgress::WritePixel(None(), None()), None())
                        } else if data == 2 {
                            (CommandProgress::WriteMany(None(), None(), None()), None())
                        } else {
                            (CommandProgress::Waiting(), None())
                        }
                    },
                    CommandProgress::WritePixel$(addr1: None(), addr2) => (
                        CommandProgress::WritePixel$(addr1: Some(data), addr2),
                        None(),
                    ),
                    CommandProgress::WritePixel$(addr1: Some(addr1), addr2: None()) => (
                        CommandProgress::WritePixel$(addr1: Some(addr1), addr2: Some(data)),
                        None(),
                    ),
                    CommandProgress::WritePixel$(addr1: Some(addr1), addr2: Some(addr2)) => (
                        CommandProgress::Waiting(),
                        Some(Command::WritePixel$(addr: addr1 `concat` addr2, value: trunc(data))),
                    ),
                    CommandProgress::WritePixel(_, _) => (CommandProgress::Waiting(), None()),
                    CommandProgress::WriteMany$(addr1: None(), addr: None(), len) => (
                        CommandProgress::WriteMany$(addr1: Some(data), addr: None(), len),
                        None(),
                    ),
                    CommandProgress::WriteMany$(addr1: Some(addr1), addr, len) => (
                        CommandProgress::WriteMany$(addr1: None(), addr: Some(addr1 `concat` data), len),
                        None(),
                    ),
                    CommandProgress::WriteMany$(addr1: None(), addr: Some(addr), len: None()) => (
                        CommandProgress::WriteMany$(addr1: None(), addr: Some(addr), len: Some(data)),
                        None(),
                    ),
                    CommandProgress::WriteMany$(addr1: None(), addr: Some(addr), len: Some(len)) => if len == 0 {
                        (
                            CommandProgress::Waiting(),
                            Some(Command::WritePixel$(addr, value: trunc(data))),
                        )
                    } else {
                        (
                            CommandProgress::WriteMany$(addr1: None(), addr: Some(trunc(addr+1)), len: Some(trunc(len - 1))),
                            Some(Command::WritePixel$(addr, value: trunc(data))),
                        )
                    },
                    CommandProgress::WriteMany(_, _, _) => (CommandProgress::Waiting(), None()),
                }
            },
            None => (progress, command),
        }
    };

    command
}

entity spi_sub_recv(
    clk: clock,
    rst: bool,
    sck: bool,
    sel: bool,
    sdi: bool,
) -> Option<uint<8>> {
    reg(clk) sck_sync reset (rst: false) = sck;
    reg(clk) sck_prev reset (rst: false) = sck_sync;
    let sck_rising = !sck_prev && sck_sync;
    let sck_falling = sck_prev && !sck_sync;

    reg(clk) sel_sync reset (rst: true) = sel;
    reg(clk) sel_prev reset (rst: true) = sel_sync;
    let sel_active = !sel_sync;
    let sel_startmsg = sel_prev && !sel_sync;
    let sel_endmsg = !sel_prev && sel_sync;

    reg(clk) sdi_sync = sdi;

    reg(clk) bitcnt: uint<4> reset (rst: 0) = {
        if !sel_active {
            0
        } else {
            if sck_rising {
                trunc(bitcnt + 1)
            } else {
                bitcnt
            }
        }
    };

    // shift-left register
    reg(clk) data: uint<8> reset (rst: 0) = {
        if sck_rising {
            trunc(data `concat` (if sdi_sync {1u1} else {0u1}))
        } else {
            data
        }
    };

    reg(clk) byte_received reset (rst: false) = sel_active && sck_rising && (bitcnt == 0b0111 || bitcnt == 0b1111);
    if byte_received {
        Some(data)
    } else {
        None()
    }
}

struct VgaOutput {
    hsync: bool,
    vsync: bool,
    red: uint<3>,
    green: uint<3>,
    blue: uint<3>,
}

struct port ColorPort {
    addr: &mut uint<14>,
    data: &uint<4>,
}

entity color_from_mem(
    clk: clock,
    color_port: ColorPort,
    pixel: (uint<15>, uint<15>),
) -> (uint<3>, uint<3>, uint<3>) {
    let (x, y) = pixel;
    let x: uint<14> = trunc(x >> 3);
    let y: uint<7> = trunc(y >> 3);
    let y_addr: uint<14> = trunc(y  * 80u8);

    set color_port.addr = trunc(y_addr + x);

    // TODO: palette memory
    let color = match *color_port.data {
        0 => (0b000, 0b000, 0b000),
        1 => (0b111, 0b000, 0b000), // red
        2 => (0b000, 0b111, 0b000), // green
        3 => (0b000, 0b000, 0b111), // blue
        _ => (0b000, 0b000, 0b000),
    };
    color
}

entity vga_top(clk: clock, rst: bool, command: Option<Command>) -> VgaOutput {
    let read_addr = inst new_mut_wire();

    let (we, write_addr, write_val): (bool, uint<14>, uint<4>) = match command {
        Some(Command::WritePixel$(addr, value)) => (true, trunc(addr), value),
        Some(Command::Dummy()) => (false, 0, 0),
        None() => (false, 0, 0),
    };

    let mem: Memory<uint<4>, 9600> = inst mem::clocked_memory(
        clk,
        [(we, write_addr, write_val)]
    );

    reg(clk) mem_data = inst std::mem::read_memory(mem, inst read_mut_wire(read_addr));

    let mem_port = ColorPort$(
        addr: read_addr,
        data: &mem_data,
    );

    let timing = vga::vga::VgaTiming$(
        x_pixels: 640,
        x_front_porch: 16,
        x_sync_width: 96,
        x_back_porch: 48,

        y_pixels: 480,
        y_front_porch: 10,
        y_sync_width: 2,
        y_back_porch: 33,
    );
    let vga_clock = true; // no clock division
    let vga_state = inst vga::vga::vga_fsm(clk, rst, vga_clock, timing);
    let output = vga::vga::vga_output(vga_state);
    let (red, green, blue) = match output.pixel {
        Some(pixel) => {
            let pixel: (int<15>, int<15>) = pixel;
            let x = pixel#0.to_uint();
            let y = pixel#1.to_uint();
            inst color_from_mem(clk, mem_port, (x, y))
        },
        None => (0, 0, 0),
    };

    VgaOutput$(
        hsync: output.hsync,
        vsync: output.vsync,
        red,
        green,
        blue,
    )
}

#[no_mangle]
entity top(
    #[no_mangle] clk: clock,
    #[no_mangle] switch: [bool; 4],
    #[no_mangle] led: &mut [bool; 4],

    // PMOD outputs
    // #[no_mangle] pmod_10: &mut bool, PMOD inputs
    #[no_mangle] pmod_1: bool,
    #[no_mangle] pmod_2: bool,
    #[no_mangle] pmod_3: bool,
    #[no_mangle] pmod_4: bool,

    #[no_mangle] vga_hsync: &mut bool,
    #[no_mangle] vga_vsync: &mut bool,
    #[no_mangle] vga_red: &mut uint<3>,
    #[no_mangle] vga_green: &mut uint<3>,
    #[no_mangle] vga_blue: &mut uint<3>,
) {
    reg(clk) rst = switch[0];

    let sck = pmod_4;
    let sdo = pmod_3;
    let sdi = pmod_2;
    let sel = pmod_1;

    let data = inst spi_sub_recv$(clk, rst, sck, sel, sdi);

    // remember the last received data
    reg(clk) data_reg reset (rst: 0) = match data {
        Some(data) => data,
        None() => data_reg,
    };

    let has_data = match data {
        Some(_) => true,
        None() => false,
    };

    reg(clk) n_received_data: uint<5> reset (rst: 0) = {
        if has_data {
            trunc(n_received_data + 1)
        } else {
            n_received_data
        }
    };

    // set led = [
    //     (n_received_data & 0b0001) != 0,
    //     (n_received_data & 0b0010) != 0,
    //     (n_received_data & 0b0100) != 0,
    //     (n_received_data & 0b1000) != 0,
    // ];
    set led = [
        (data_reg & 0b0001) != 0,
        (data_reg & 0b0010) != 0,
        (data_reg & 0b0100) != 0,
        (data_reg & 0b1000) != 0,
    ];

    let command = inst command_fsm(clk, rst, data);

    let VgaOutput$(hsync, vsync, red, green, blue) = inst vga_top(clk, rst, command);
    set vga_hsync = hsync;
    set vga_vsync = vsync;
    set vga_red = red;
    set vga_green = green;
    set vga_blue = blue;
}
